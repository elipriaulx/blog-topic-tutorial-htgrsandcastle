﻿using System;
using System.Collections.Generic;
using System.Linq;
using SandcastleHTGR.BasicLibrary.Borrowables;
using SandcastleHTGR.BasicLibrary.Contracts;

namespace SandcastleHTGR.BasicLibrary
{
    /// <summary>
    ///  A simple library intended for personal use, which facilitates storage and management of basic articles, but lacks searching.
    /// </summary>
    public class PersonalLibrary : ISimpleLibrary
    {
        private readonly List<IArticle> _articles;

        /// <summary>
        ///  Construct a new PersonalLibrary
        /// </summary>
        public PersonalLibrary()
        {
            _articles = new List<IArticle>();
        }

        /// <summary>
        /// Construct a new PersonalLibrary from an existing library's collection.
        /// </summary>
        /// <param name="existingLibrary">An enumerable IArticle collection.</param>
        public PersonalLibrary(IEnumerable<IArticle> existingLibrary)
        {
            _articles = new List<IArticle>();

            foreach (var article in existingLibrary) Add(article.Clone());
        }

        /// <summary>
        /// Retreive the count of articles in the library.
        /// </summary>
        public int ArticleCount => _articles.Count;

        /// <summary>
        /// Add an article to the library.
        /// </summary>
        /// <param name="article">An article that could be anything from a book to a magazine.</param>
        /// <returns>A boolean value expressing the method's ability to add something to the collection.</returns>
        public bool Add(IArticle article)
        {
            if (_articles.Any(x => article.Identifier == x.Identifier)) return false;

            _articles.Add(article);

            return true;
        }

        /// <summary>
        /// Remove an article from the library using its object reference.
        /// </summary>
        /// <param name="article">A reference to the article to remove.</param>
        /// <returns>A boolean value representing the success of the removal request. A false may equally indicate a failure to remove, and the item not existing in the first place.</returns>
        public bool Remove(IArticle article)
        {
            return _articles.Remove(article);
        }

        /// <summary>
        /// Remove an article from the library using its unique identifier.
        /// </summary>
        /// <param name="articleId">A Guid matching the target article.</param>
        /// <returns>A boolean value representing the success of the removal request. A false may equally indicate a failure to remove, and the item not existing in the first place.</returns>
        public bool Remove(Guid articleId)
        {
            var removable = _articles.FirstOrDefault(x => x.Identifier == articleId);

            return removable != null && _articles.Remove(removable);
        }

        /// <summary>
        /// Get an enumerable collection of all the articles in the library.
        /// </summary>
        /// <returns>An IEnumerable collection of IArticles derived from the library.</returns>
        public IEnumerable<IArticle> GetArticles()
        {
            return _articles;
        }

        /// <summary>
        /// Get an enumerable collection of the books in the library.
        /// </summary>
        /// <returns>An IEnumerable collection of IBooks derived from the library.</returns>
        public IEnumerable<IBook> GetBooks()
        {
            return _articles.OfType<IBook>();
        }

        /// <summary>
        /// Get an enumerable collection of all the magazines in the library.
        /// </summary>
        /// <returns>An IEnumerable collection of Magazines derived from the library.</returns>
        public IEnumerable<Magazine> GetMagazines()
        {
            return _articles.OfType<Magazine>();
        }

        /// <summary>
        /// Get an enumerable collection of all the Fiction books in the library.
        /// </summary>
        /// <returns>An IEnumerable collection of FicticiousBooks derived from the library.</returns>
        public IEnumerable<FicticiousBook> GetFicticiousBooks()
        {
            return _articles.OfType<FicticiousBook>();
        }

        /// <summary>
        /// Get an enumerable collection of all the NonFiction books in the library.
        /// </summary>
        /// <returns>An IEnumerable collection of AllegedlyTrueBooks derived from the library.</returns>
        public IEnumerable<AllegedlyTrueBook> GetAllegedlyTrueBook()
        {
            return _articles.OfType<AllegedlyTrueBook>();
        }
    }
}
