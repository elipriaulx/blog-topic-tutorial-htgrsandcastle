﻿using System;
using SandcastleHTGR.BasicLibrary.Contracts;
using SandcastleHTGR.BasicLibrary.Types;

namespace SandcastleHTGR.BasicLibrary.Borrowables
{
    /// <summary>
    /// A Magazine Article represents a physical magazine's identity in object form, carrying many of its attributes.
    /// </summary>
    public class Magazine : IArticle
    {
        /// <summary>
        /// Construct a new magazine, and cateorise it.
        /// </summary>
        public Magazine()
        {
            Identifier = Guid.NewGuid();
            Category = CategoryTypes.Magazine;
        }

        /// <summary>
        /// An identifier that should be globally unique to this book.
        /// </summary>
        public Guid Identifier { get; set; }

        /// <summary>
        /// The common title of the book.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// A string collection containing the authors of the publication. Ordered by importance.
        /// </summary>
        public string[] Authors { get; set; }

        /// <summary>
        /// The publishing company name of this book.
        /// </summary>
        public string Publisher { get; set; }

        /// <summary>
        /// A crude classification used to describe the nature of this article.
        /// </summary>
        public CategoryTypes Category { get; set; }

        /// <summary>
        /// A collection denoting the free things bundled to the front cover of the magazine, such as samples of useless items, and knick-knacks.
        /// </summary>
        public string[] FreeJunkOnCover { get; set; }

        /// <summary>
        /// A collection containing the irrational claims on the cover - the tag lines that aim to make you want to read articles.
        /// </summary>
        public string[] IrrationalClaimsOnCover { get; set; }

        /// <summary>
        /// Copy this magazine to a new article.
        /// </summary>
        /// <returns>A clone is returned, with a unique identifier.</returns>
        public IArticle Clone()
        {
            var n = new Magazine
            {
                Authors = (string[])Authors.Clone(),
                Category = Category,
                FreeJunkOnCover = (string[])FreeJunkOnCover.Clone(),
                IrrationalClaimsOnCover = (string[])IrrationalClaimsOnCover.Clone(),
                Publisher = Publisher,
                Title = Title
            };

            return n;
        }
    }
}
