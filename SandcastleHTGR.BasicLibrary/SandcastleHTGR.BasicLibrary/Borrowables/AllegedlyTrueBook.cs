﻿using System;
using SandcastleHTGR.BasicLibrary.Contracts;
using SandcastleHTGR.BasicLibrary.Types;

namespace SandcastleHTGR.BasicLibrary.Borrowables
{
    /// <summary>
    /// A book containing information that is assumed to be of the Non-Fiction variety.
    /// </summary>
    public class AllegedlyTrueBook : IBook
    {
        /// <summary>
        /// Construct a new AllegedyTrueBook and categorise it as Non-Fiction
        /// </summary>
        public AllegedlyTrueBook()
        {
            Identifier = Guid.NewGuid();
            Category = CategoryTypes.NonFiction;
        }

        private float _credibility;

        /// <summary>
        /// A string array tagging topics covered in this book.
        /// </summary>
        public string[] Topics { get; set; }

        /// <summary>
        /// An identifier that should be globally unique to this book.
        /// </summary>
        public Guid Identifier { get; set; }

        /// <summary>
        /// The common title of the book.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// A string collection containing the authors of the publication. Ordered by importance.
        /// </summary>
        public string[] Authors { get; set; }

        /// <summary>
        /// The publishing company name of this book.
        /// </summary>
        public string Publisher { get; set; }

        /// <summary>
        /// A crude classification used to describe the nature of this article.
        /// </summary>
        public CategoryTypes Category { get; set; }

        /// <summary>
        /// This string expresses the Internation Standard Book Number applicable to this article.
        /// </summary>
        public string Isbn { get; set; }

        /// <summary>
        /// With how polluted the average book collection has become, it is entierly necessary to judge a book by its cover. A blurb helps describe the contents of a book with a friendly description of what lies within.
        /// </summary>
        public string Blurb { get; set; }

        /// <summary>
        /// Cover reflects the type of cover physcially present on the publication.
        /// </summary>
        public CoverTypes Cover { get; set; }

        /// <summary>
        /// A float from 0 to 1 expressing the percentage of credability of this article.
        /// </summary>
        public float Credibility
        {
            get { return _credibility; }
            set
            {
                if (value < 0 || value > 1.0f) throw new ArgumentOutOfRangeException();
                _credibility = value;
            }
        }

        /// <summary>
        /// Copy this book to a new article.
        /// </summary>
        /// <returns>A clone is returned, with a unique identifier.</returns>
        public IArticle Clone()
        {
            var n = new AllegedlyTrueBook
            {
                Authors = (string[])Authors.Clone(),
                Blurb = Blurb,
                Category = Category,
                Cover = Cover,
                Isbn = Isbn,
                Publisher = Publisher,
                Title = Title,
                Credibility = Credibility,
                Topics = (string[])Authors.Clone()
            };

            return n;
        }
    }
}
