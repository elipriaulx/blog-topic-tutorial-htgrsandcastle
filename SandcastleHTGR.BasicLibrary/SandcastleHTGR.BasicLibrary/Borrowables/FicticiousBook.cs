﻿using System;
using SandcastleHTGR.BasicLibrary.Contracts;
using SandcastleHTGR.BasicLibrary.Types;

namespace SandcastleHTGR.BasicLibrary.Borrowables
{
    /// <summary>
    /// A book known to be ficticious, such as a novel.
    /// </summary>
    public class FicticiousBook : IBook
    {
        /// <summary>
        /// Construct a new ficticious book and categorise it accordingly.
        /// </summary>
        public FicticiousBook()
        {
            Identifier = Guid.NewGuid();
            Category = CategoryTypes.Fiction;
        }

        /// <summary>
        /// An identifier that should be globally unique to this book.
        /// </summary>
        public Guid Identifier { get; set; }

        /// <summary>
        /// The common title of the book.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// A string collection containing the authors of the publication. Ordered by importance.
        /// </summary>
        public string[] Authors { get; set; }

        /// <summary>
        /// The publishing company name of this book.
        /// </summary>
        public string Publisher { get; set; }

        /// <summary>
        /// A crude classification used to describe the nature of this article.
        /// </summary>
        public CategoryTypes Category { get; set; }

        /// <summary>
        /// This string expresses the Internation Standard Book Number applicable to this article.
        /// </summary>
        public string Isbn { get; set; }

        /// <summary>
        /// With how polluted the average book collection has become, it is entierly necessary to judge a book by its cover. A blurb helps describe the contents of a book with a friendly description of what lies within.
        /// </summary>
        public string Blurb { get; set; }

        /// <summary>
        /// Cover reflects the type of cover physcially present on the publication.
        /// </summary>
        public CoverTypes Cover { get; set; }

        /// <summary>
        /// Copy this book to a new article.
        /// </summary>
        /// <returns>A clone is returned, with a unique identifier.</returns>
        public IArticle Clone()
        {
            var n = new FicticiousBook
            {
                Authors = (string[])Authors.Clone(),
                Blurb = Blurb,
                Category = Category,
                Cover = Cover,
                Isbn = Isbn,
                Publisher = Publisher,
                Title = Title
            };

            return n;
        }
    }
}
