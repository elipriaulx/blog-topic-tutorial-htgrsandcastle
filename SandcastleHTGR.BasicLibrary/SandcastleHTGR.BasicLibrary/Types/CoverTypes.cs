﻿namespace SandcastleHTGR.BasicLibrary.Types
{
    /// <summary>
    /// Cover Types descibe the material used to protect the faces of the outer pages of a book.
    /// </summary>
    public enum CoverTypes
    {
        /// <summary>
        /// Hardcover represents a solid exterior shell.
        /// </summary>
        HardCover = 0,

        /// <summary>
        /// SoftCover denotes an exterior shell flacid in nature.
        /// </summary>
        SoftCover = 1
    };
}
