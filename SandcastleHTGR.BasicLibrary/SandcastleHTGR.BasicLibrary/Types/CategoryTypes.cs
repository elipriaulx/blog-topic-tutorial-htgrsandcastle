﻿namespace SandcastleHTGR.BasicLibrary.Types
{
    /// <summary>
    /// Category Types help categorise IArticle content.
    /// </summary>
    public enum CategoryTypes
    {
        /// <summary>
        /// Other is used to categorise all items not specifically otherwise captured.
        /// </summary>
        Other = 0,

        /// <summary>
        /// Fiction is used to denote an article with content that is best described as not true.
        /// </summary>
        Fiction = 10,

        /// <summary>
        /// NonFiction is used to denote an article with content that is assumed to be true.
        /// </summary>
        NonFiction = 11,

        /// <summary>
        /// Magazine is used to denote an article which is akin to a magaine in appearance, format, and value.
        /// </summary>
        Magazine = 20,
    }
}
