﻿using System;
using SandcastleHTGR.BasicLibrary.Types;

namespace SandcastleHTGR.BasicLibrary.Contracts
{
    /// <summary>
    /// An IArticle defines the minimum requirements of something to be borrowable. All borrowables ultimatley implement IArticle.
    /// </summary>
    public interface IArticle
    {
        /// <summary>
        /// The Aurthors Array lists the contributors to the article. It is assumed that the earlier an author appears in the array, the more important their contribution to the work.
        /// </summary>
        string[] Authors { get; set; }

        /// <summary>
        /// Category represents a crude classification for all articles.
        /// </summary>
        CategoryTypes Category { get; set; }

        /// <summary>
        /// The article's identifier should be unique to any library or collection. Think of it as your primary key.
        /// </summary>
        Guid Identifier { get; set; }

        /// <summary>
        /// A singular Publishers name may be listed against each article.
        /// </summary>
        string Publisher { get; set; }

        /// <summary>
        /// Every article should have a title for easy identification.
        /// </summary>
        string Title { get; set; }

        /// <summary>
        /// The Clone method returns a new article identical in type and value to an existing article.
        /// </summary>
        /// <returns></returns>
        IArticle Clone();

    }
}
