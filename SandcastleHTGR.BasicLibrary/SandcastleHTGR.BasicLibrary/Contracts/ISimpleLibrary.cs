﻿using System;
using System.Collections.Generic;
using SandcastleHTGR.BasicLibrary.Borrowables;

namespace SandcastleHTGR.BasicLibrary.Contracts
{
    /// <summary>
    ///  A simple library facilitates storage and management of basic articles, but lacks searching.
    /// </summary>
    public interface ISimpleLibrary
    {
        /// <summary>
        /// Get the quantity of articles contained within this Simple Library.
        /// </summary>
        int ArticleCount { get; }

        /// <summary>
        /// Add a new IArticle to the library.
        /// </summary>
        /// <param name="article">The article of which to add to this collection.</param>
        /// <returns>A boolean value indicating the success of the Add function. An add will fail if the article's identifier is not unique in the collection.</returns>
        bool Add(IArticle article);

        /// <summary>
        /// Remove an article from the library using the object reference.
        /// </summary>
        /// <param name="article">A reference to the IArticle to be removed.</param>
        /// <returns>A boolean value indicating success of the removal. This will return false if the IArticle does not exist within the collection or removal fails.</returns>
        bool Remove(IArticle article);

        /// <summary>
        /// Remove an article from the library using the unique Guid of IArticle.
        /// </summary>
        /// <param name="articleId">A Guid expressing the IArticle's unique identifier.</param>
        /// <returns>A boolean value indicating success of the removal. This will return false if the IArticle does not exist within the collection or removal fails.</returns>
        bool Remove(Guid articleId);

        /// <summary>
        /// Retreive all the IArticles from the Library.
        /// </summary>
        /// <returns>An enumerable collection of IArticles.</returns>
        IEnumerable<IArticle> GetArticles();

        /// <summary>
        /// Retreive all the IBooks from the Library.
        /// </summary>
        /// <returns>An enumerable collection of IBooks.</returns>
        IEnumerable<IBook> GetBooks();

        /// <summary>
        /// Retreive all the Magazines from the Library.
        /// </summary>
        /// <returns>An enumerable collection of Magazines.</returns>
        IEnumerable<Magazine> GetMagazines();

        /// <summary>
        /// Retreive all the Fiction Books from the Library.
        /// </summary>
        /// <returns>An enumerable collection of FicticiousBooks.</returns>
        IEnumerable<FicticiousBook> GetFicticiousBooks();

        /// <summary>
        /// Retreive all the Non Fiction from the Library.
        /// </summary>
        /// <returns>An enumerable collection of AllegedlyTrueBooks.</returns>
        IEnumerable<AllegedlyTrueBook> GetAllegedlyTrueBook();
    }
}
