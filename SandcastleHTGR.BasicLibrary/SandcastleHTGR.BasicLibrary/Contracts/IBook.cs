﻿using SandcastleHTGR.BasicLibrary.Types;

namespace SandcastleHTGR.BasicLibrary.Contracts
{
    /// <summary>
    /// An IBook extends an IArticle, adding more specifics in the book paradigm.
    /// </summary>
    public interface IBook: IArticle
    {
        /// <summary>
        /// This string expresses the Internation Standard Book Number applicable to this article.
        /// </summary>
        string Isbn { get; set; }

        /// <summary>
        /// With how polluted the average book collection has become, it is entierly necessary to judge a book by its cover. A blurb helps describe the contents of a book with a friendly description of what lies within.
        /// </summary>
        string Blurb { get; set; }

        /// <summary>
        /// Cover reflects the type of cover physcially present on the publication.
        /// </summary>
        CoverTypes Cover { get; set; }
    }
}
